#!/bin/bash
### получаем ID последней успешной джобы в build 
LASTJOBID=$(curl --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/22610538/jobs" | jq -r '[.[] | select((.stage=="build") and (.pipeline.status=="success"))][0] | .id')
echo $LASTJOBID
### Получаем артефакт из последней успешной джобы
TSHOOTVAR=$(curl --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/22610538/jobs/$LASTJOBID/artifacts/build_master_log.txt" --output ./build_master_log.txt)
